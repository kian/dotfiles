execute pathogen#infect()

let mapleader = ','

set background=dark
let g:rehash256 = 1
colorscheme molokai

let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 0
let g:airline_theme = 'badwolf'

set expandtab
set shiftwidth=4 softtabstop=4 tabstop=4
set textwidth=80
set nobackup
set number
set relativenumber
set noshowmode " handled by vim-airline

au Filetype yaml,html,ruby,javascript setlocal ts=2 sts=2 sw=2
au BufNewFile,BufRead [vV]agrantfile set filetype=ruby

" show matching brackets
set showmatch
" show for 0.2 seconds
set matchtime=2
" match <> tags (HTML, XML, ...)
set matchpairs+=<:>

" disable arrow keys
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>
imap <up> <nop>
imap <down> <nop>
imap <left> <nop>
imap <right> <nop>

map <C-n> :NERDTreeToggle<CR>
map t :NERDTreeToggle<CR>

nnoremap <f5> :set relativenumber!<CR>
nnoremap <f6> :set nonumber!<CR>
