if status is-interactive
    # Commands to run in interactive sessions can go here
    pyenv init - | source

    starship init fish | source
end
